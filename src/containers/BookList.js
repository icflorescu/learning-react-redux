import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { selectBook } from '../actions';

class BookList extends Component {
  render() {
    return (
      <ul>
        {this.props.books.map(book =>
          <li key={book.index} onClick={() => this.props.selectBook(book)}>
            {book.title}
          </li>
        )}
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return { books: state.books };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ selectBook }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
