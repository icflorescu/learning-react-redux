import React, { Component } from 'react';
import { connect } from 'react-redux';

const BookDetails = props => {
  if (!props.book) return <div>Select a book to get started.</div>;
  return (
    <div>
      <h2>{props.book.title}</h2>
      <p>Pages: {props.book.pages}</p>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    book: state.activeBook
  };
};

export default connect(mapStateToProps)(BookDetails);
