import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import reducers from './reducers';
import App from './components/App';

import './index.scss';

const store = applyMiddleware()(createStore)(reducers);
window.store = store;

if (process.env.NODE_ENV == 'production') {
  ReactDOM.render(
    <Provider store={store}><App /></Provider>,
    document.getElementById('app'));
} else {
  const AppContainer = require('react-hot-loader').AppContainer;
  const render = () => {
    ReactDOM.render(
      <Provider store={store}><AppContainer><App /></AppContainer></Provider>,
      document.getElementById('app')
    );
  };
  if (module.hot) {
    module.hot.accept('./reducers', () => { store.replaceReducer(reducers); });
    module.hot.accept('./components/App', render);
  }
  render();
}
