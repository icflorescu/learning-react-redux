import React from 'react';

import Title       from './Title';
import BookList    from '../containers/BookList';
import BookDetails from '../containers/BookDetails';

export default props => (
  <div>
    <Title />
    <BookList />
    <hr />
    <BookDetails />
  </div>
);
